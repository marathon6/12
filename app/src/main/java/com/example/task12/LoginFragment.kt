package com.example.task12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView

class LoginFragmet : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login_fragmet, container, false)

        val tvGoToRegisterFragment = view.findViewById<TextView>(R.id.tvGoToRegisterFragment)
        val tvSkipLogin = view.findViewById<TextView>(R.id.tvSkipLogin)



        tvGoToRegisterFragment.setOnClickListener { Navigation.findNavController(view).navigate(R.id.action_loginFragmet_to_registrationFragment) }
        tvSkipLogin.setOnClickListener { Navigation.findNavController(view).navigate(R.id.action_loginFragmet_to_homeFragment) }



        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragmet()
    }
}