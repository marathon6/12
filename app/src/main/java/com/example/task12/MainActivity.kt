package com.example.task12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment()
        val webFragment = WebFragment()
        val settingsFragment = SettingsFragment()

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)

        bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.miHome -> setCurrentFragment(homeFragment)
                R.id.miWeb -> setCurrentFragment(webFragment)
                R.id.miSettings -> setCurrentFragment(settingsFragment)
            }
            true
        }

        //bottomNavigationView.isVisible = false
        //setupNav()

        // не получилось у меня к сожалению в этом задании скрывать навигацию на логине и регистрации
        // в любом случае это придется сделать для 13 задния, но сейчас уже просто не успею до дедлайна :(
    }

    private fun setCurrentFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

    /*private fun setupNav() {
        val navController = findNavController(R.id.navigation_controller)
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNav.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.loginFragmet -> bottomNav.isVisible = false
                R.id.registrationFragment -> bottomNav.isVisible = false
                else -> bottomNav.isVisible = true
            }
        }
    }*/
}