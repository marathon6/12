package com.example.task12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation

class RegistrationFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        val btnGoToLoginFragmet = view.findViewById<Button>(R.id.btnGoToLoginFragment)

        btnGoToLoginFragmet.setOnClickListener { Navigation.findNavController(view).navigate(R.id.action_registrationFragment_to_loginFragmet) }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}